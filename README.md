
**Team(郏高阳，赵长永) **

**why easy-ajax** 

easy-ajax是为了治理前端乱写Ajax方法而生。基于jquery封装一些常用方法，后期计划加入前端数据缓存功能。

 **使用方法如下：** 
```
<script src="easy.ajax.js"></script>
```
 **API：** 
```
config配置项：

config{
    jqueryAjax默认配置外增加如下配置：
    mustCallback：强制回调（很多情况请求失败后直接在Ajax中提示后端返回的错误信息就结束了，不会进入回调函数，但很难避免失败也需要处理逻辑的情况，所以增加了此项配置，无论成功失败皆执行配置中的callback，前提是有回掉函数。）
}
```
使用from表单方式处理参数请求后台（接口使用@RequestParam时使用）
```
EasyAjax.post_form_json
```
使用普通方式请求（接口使用@RequestBody时使用）
```
EasyAjax.post_json
```

文件上传方法1：
```
var file = $(".xxx").get(0).files[0];
var fileData = new FormData();
fileData.append("file", file);
EasyAjax.ajax_Upload_File({
                    url: "URL",
                    data: fileData
                },
                function (data) {
                    if (data.success) {
                        //上传成功
                    }
                });

```
文件上传方法2：直接配置elem方法，使用方式如下：
```
EasyAjax.ajax_Upload_File_Elem({
                    url:"URL"
                    , elem: "#icon"
                },function (res) {
                    if (res.success) {
                        //上传成功
                    }
                });
```

有任何问题或者建议可以留言，大家一起努力为开源做一点小贡献。让开发更便捷，更易拓展！

 